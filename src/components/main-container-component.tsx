import React, { useState } from 'react';
import { Button, Col, Row } from 'antd/lib';
import IntegrationEntity from './integration-entity-component';
import SyncFromLeft from '../icons/SyncFromLeft';
import SyncFromRight from '../icons/SyncFromRight';
import Gmail from '../icons/Gmail';
import MailChimp from '../icons/MailChimp';
import './main-container-component.css'


interface MainContainerProps {
  readonly title: string;
}

export const MainContainerContext = React.createContext<any>({});

const MainContainer: React.FC<MainContainerProps> = ({ title }) => {

  const [leftSender, setLeftSender] = useState<boolean>(true);
  const [rightSender, setRightSender] = useState<boolean>(false);

   const swithSender = () => {
     setLeftSender(!leftSender)
     setRightSender(!rightSender)
   }

  
    return (
        <div className='main-div'>
          <Row  gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} justify='space-around' align='middle'>
              <Col span={8}>
                <IntegrationEntity title='Gmail' entityToSync='MailChimp' isSender={leftSender} logo={<Gmail />}/>
              </Col>
              <Col span={4}>
                <Button ghost shape='circle' onClick={swithSender}>
                  {leftSender ? <SyncFromLeft />: <SyncFromRight />}
                </Button>
              </Col>
              <Col span={8}>
                <IntegrationEntity title='MailChimp' entityToSync='Gmail' isSender={rightSender} logo={<MailChimp />}/>
              </Col>
          </Row>
        </ div>
    );

};

export default MainContainer;