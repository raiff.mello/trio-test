import React  from 'react';
import { Card, Row } from 'antd';
import { Select } from 'antd';

const { Option } = Select;

const childrens: ReadonlyArray<any> = [
  <Option key='1' value={'Family'}>Family</Option>,
  <Option key='2' value={'Work Friends'}>Work Friends</Option>,
  <Option key='3' value={'Another Friends'}>Another Friends</Option>
];

function handleChange(value: string) {
  console.log(`selected ${value}`);
}


interface IntegrationEntityProps {
  readonly title: string;
  readonly entityToSync:string;
  readonly isSender: boolean;
  readonly logo: any;
}

const IntegrationEntity: React.FC<IntegrationEntityProps> = ({ title, entityToSync, isSender, logo }) => {
  
    return (
      <>
        <Card style={{width: 285}}>
          {logo}
          <Row style={{fontWeight:500, fontSize:18}}  gutter={16} justify='space-around' align='middle'>
            {title}
          </Row>
          <br/>
          <Row  gutter={16} justify='space-around' align='middle'>
            These {title} contacts will<br/> sync to {entityToSync}
          </Row>
          <br/>
          <Row>
            <>
              <Select
                mode="multiple"
                allowClear
                style={{ width: '100%' }}
                placeholder="Please select"
                onChange={handleChange}
                disabled={!isSender}
              >
                {childrens}
              </Select>
              <br />
            </>
          </Row>
        </Card>
      </>
    );

};

export default IntegrationEntity;