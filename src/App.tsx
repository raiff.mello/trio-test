import './App.css';
import MainContainer from './components/main-container-component';

function App() {
  return (
    <div className="App">
      <MainContainer title={'Trio Test'} />
    </div>
  );
}

export default App;
